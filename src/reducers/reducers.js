import { combineReducers } from 'redux';

import MenuReducer from './menu_reducer';



const rootReducer = combineReducers({
    menu : MenuReducer,
});

export default rootReducer;